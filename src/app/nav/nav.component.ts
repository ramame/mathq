import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { TeacherdetailsComponent } from '../teacherdetails/teacherdetails.component';
@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  constructor(public router:Router, public auth:AuthService, public dialog: MatDialog) {
    
  } 
  openDialog(): void {
    const dialogRef = this.dialog.open(TeacherdetailsComponent, {
      height: '30%',
      width: '10%px',
      position:{
        'top' : '5.5%',
        'right' : '3.5%'
      }
    }
  );
  }

  ngOnInit() {
  }
openpop(){

}
EnterPIN(){
  this.router.navigate(['/gamepin']);
}
  toHome(){
  this.router.navigate(['/home']);
  }
  toRegister(){
    this.router.navigate(['/register']);
  }
  toLogin(){
    this.router.navigate(['/login']);
  }
  toLogout(){
    this.auth.logout();
  }
  toReport(){
    this.router.navigate(['/reports']);
  }
  toExams(){
    this.router.navigate(['/exams']);
  }


}
