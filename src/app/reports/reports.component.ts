import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { ExamService } from '../exam.service';


export interface PeriodicElement {
  key: string;
  timeLeft: number;
  counter: number;
}


export interface exams {
  key:any, endTime:any, examName:any, numQue:any,questions:any,students:any
}


@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})

export class ReportsComponent implements OnInit {
 
  exams = [];
  students:PeriodicElement|any ;
  fixTable = [];
  displayedColumns: string[] = ['key', 'timeLeft', 'counter'];
  dataSource : PeriodicElement[];
  numberOfQuestion: any;
  endTime: any;
  myExam:exams|any =[];
  showCard:boolean;
  showCard2:boolean;
  //displayColumns is Exercise and then each names
  
  constructor(public Auth:AuthService, public exam:ExamService) { }



  openReport(key){
         this.showCard2 = false
        this.exams.forEach(value => {
          if(value.key === key){
          this.endTime = value.endTime;
          this.numberOfQuestion = value.numQue;
          }});
        
       
        
        this.Auth.user.subscribe(user => {
        this.exam.readForReport(user, key).subscribe(
        students =>{                                                  
        this.students = [];                
        students.forEach(                   
        student => {                          
        let y = student.payload.toJSON();                          
        y["key"] = student.key;                                                
        this.students.push(y);    
        })})})
      this.delay(400).then(value => {
        this.dataSource = this.students;
      })
       
  }

  ngOnInit() {
   
    this.delay(2000).then(value =>{
      console.log(this.exams.length)
      if(this.exams.length == 0){
        this.showCard = true;
        this.showCard2 = false;
      }
      else{
        this.showCard = false;
        this.showCard2 = true;
      }
    })
 
    //read exams by servise
     this.dataSource = null;
     this.Auth.user.subscribe(value => {
     this.exam.readExamForReport(value).subscribe(
     exams =>{                                                  
     this.exams = [];                
     exams.forEach(                   
     exam => {                          
     let y = exam.payload.toJSON();                          
     y["key"] = exam.key;                                                 
     this.exams.push(y);  
     })
     })})
   }
   delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
}
}
