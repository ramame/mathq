import { Component, OnInit, OnDestroy } from '@angular/core';
import { StudentsideService } from '../studentside.service';
import { timer, Observable } from 'rxjs';
import { Router } from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';
import { element } from '@angular/core/src/render3';

@Component({
  selector: 'app-testrun',
  templateUrl: './testrun.component.html',
  styleUrls: ['./testrun.component.css']
})


export class TestrunComponent implements OnInit  {
  timeLeft: number = 60;
  interval;
  exams: {code:any, description:any, gameon:any, participants:any, questions:any, timeLimit:any, title: any; teacherID: any; key: any; };
  questions = [];
  answer = [];
  participants = [];
  jsonAnswers =[];
  i: number;
  minute: string | number;
  sec : number ;
  timeAtstart: number;
  timeToEnd:number;
  subscription;
  source: Observable<number>; 
examTitle:string;
  openSnackBarThanks() {
    this._snackBar.open("Your answers has been sent", 'Thanks', {
      duration: 2000,
    });
  }

  constructor(public studentServie:StudentsideService, public router:Router, public _snackBar: MatSnackBar) { }
  consoleAnswer(){
    console.log(this.answer)
  }

  ngOnDestroy() {
    console.log(this.exams)
    this.studentServie.logOutName();
    this.subscription.unsubscribe();
    this.openSnackBarThanks()
  }

saveAnswers(){
  this.jsonAnswers = [];
  this.questions.forEach(question => {
    let z = question;
    if(!(this.answer[question.indx] == null)){ 
    z["answer"] = this.answer[question.indx]
    }
    else{
    z["answer"] = Number(question.to)+10;
    }
    this.jsonAnswers.push(z);
  });
  this.delay(800).then(value => {
    let counter = 0 ;
    this.jsonAnswers.forEach(element => {
      let var1:number = Number(element.from)
      let var2:number = Number(element.to)
      let varanswer:number = Number(element.answer)
      if(var1 < varanswer && varanswer < var2){
        counter = counter + 1
      }
      this.studentServie.saveAnswers(element)
      this.studentServie.saveAnswersForReport(this.exams.title, this.exams.teacherID , this.exams.key , this.studentServie.myName, element, counter,this.timeToEnd,this.timeAtstart,this.questions.length)
        this.router.navigate(['/gamepin']);
       
    });
    this.studentServie.logOutName();
  })
}
  ngOnInit() {
    this.timeAtstart = this.studentServie.examjson[0].timeLimit;
    console.log(this.timeAtstart)
    this.oberserableTimer()
    this.i = 0;        
    this.exams = this.studentServie.examjson[0];
    this.examTitle = this.studentServie.examjson[0].title;
    this.studentServie.readQuestions().subscribe(questions => {
    this.questions = []
  
    questions.forEach(question =>{
      let y = question.payload.toJSON();
      y["key"] = question.key;
      y["indx"] = this.i
      this.questions.push(y)
      this.i = this.i + 1;
    })
    })

    this.studentServie.readParticipants().subscribe(participants => {
      this.participants = [];
      participants.forEach(participant =>{
  
          let y = participant.payload.toJSON();
          
          y["key"] = participant.key;
          this.participants.push(y)
       
      
      })
      })
   
    
  }
  
    oberserableTimer() {
    //timer first argument is the first emit in this section 1 second , second argument its each time that happend, this case each second
    let source = timer(1000, 1000);
   
    this.subscription = source.subscribe(val => {
    if ((this.timeAtstart - val)/60 > 1) {
      this.minute = Math.floor(this.studentServie.examjson[0].timeLimit/60);
    }
    else{
      this.minute = '00';
    }
    this.sec = (this.timeAtstart-val)%60;
    this.timeToEnd = this.timeAtstart-val;
    //val start from 1 and up
    this.studentServie.TimeForUser(val)
    //this.subscribeTimer is time left to end
    if(this.timeToEnd == 0){
    this.saveAnswers()
    }
  });
}
  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
}

}
