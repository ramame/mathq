import { Component, OnInit, Input} from '@angular/core';
import { ExamService } from '../exam.service';
import { MatDialog } from '@angular/material';
import { DeletequestionComponent } from '../deletequestion/deletequestion.component';
@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {
  @Input() dataq:any;
  @Input() count:any;
  from:number;
  to:number;
  title:string;
  from1:number;
  to1:number;
  title1:string;
  key:string;
  num:number;
  hover:boolean;
  updateform:boolean;
  testkey:string;
  constructor(public exam:ExamService, public dialog: MatDialog) { }
mouseIn()
{
this.hover = true;

}
mouseOut(){
this.hover = false;

}
deleteQuestion(){
  const dialogRef = this.dialog.open(DeletequestionComponent, {
    data: {key: this.key,  testkey: this.testkey, name: this.title}
  });

}
formMode(){
this.updateform = true;
}

regularMode(){
this.updateform = false;
this.title = this.title1;
this.to = this.to1;
this.from = this.from1;
}

updateQuetion(){
this.exam.updateQuestion(this.key, this.testkey, this.title, this.from, this.to)
}
  ngOnInit() {
    this.testkey = this.dataq.testkey;
    this.updateform = false;
    this.hover = false;
  this.num = this.count+1 ;
  console.log(this.count)
  this.key = this.dataq.key;
  this.from = this.dataq.from;
  this.to = this.dataq.to;
  this.title = this.dataq.title;
  this.from1 = this.dataq.from;
  this.to1 = this.dataq.to;
  this.title1 = this.dataq.title;
  }

}
