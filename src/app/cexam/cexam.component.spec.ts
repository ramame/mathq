import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CexamComponent } from './cexam.component';

describe('CexamComponent', () => {
  let component: CexamComponent;
  let fixture: ComponentFixture<CexamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CexamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CexamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
