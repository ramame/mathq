import { Component, OnInit } from '@angular/core';
import { ExamService } from '../exam.service';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { HttpClient } from '@angular/common/http';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-cexam',
  templateUrl: './cexam.component.html',
  styleUrls: ['./cexam.component.css']
})
export class CexamComponent implements OnInit {
title =''; 
description= '';
code;
dbpin;
selectedFile = null;
examTitle = new FormControl('', [Validators.required]);
examTime = new FormControl('', [Validators.required, Validators.pattern("^[0-9]*$")]);
timelimit; 

getErrorMessage() {
  return this.examTime.hasError('required') ? 'You must enter time for the exam (in seconds)' :
      this.examTime.hasError('pattern') ? 'Please enter a numeric time for the exam (in seconds)' :
          '';
}

getErrorMessage1() {
  return this.examTime.hasError('required') ? 'You must enter name for the exam' :
  
          '';
}

onFileSelect(event){
  this.selectedFile = event.target.files[0];
  console.log(this.selectedFile)
}

addTest(){
  if(!(this.title == '')&&!(isNaN(this.timelimit))){ 
  //randon pin generte
  var text = ""
  var possible = "0123456789";
  for (var i = 0; i < 6; i++){
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  this.code = text;
  this.auth.user.subscribe(value =>{
  this.exams.addExam(this.timelimit, this.title, this.description, this.code, value).then(
    value => {
      this.dbpin = value.key;
      this.exams.Exampin(value.key, this.title); 
      this.router.navigate(['/cquestion']);
    }
  )})
}
else{
  alert('Please try again and follow the instructions')
}
}

  constructor(public exams:ExamService, private router:Router, public auth:AuthService, public http:HttpClient) { }

  ngOnInit() {
   
  }

}
