import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TyexamComponent } from './tyexam.component';

describe('TyexamComponent', () => {
  let component: TyexamComponent;
  let fixture: ComponentFixture<TyexamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TyexamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TyexamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
