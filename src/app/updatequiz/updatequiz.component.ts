import { Component, OnInit,  Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ExamService } from '../exam.service';

@Component({
  selector: 'app-updatequiz',
  templateUrl: './updatequiz.component.html',
  styleUrls: ['./updatequiz.component.css']
})
export class UpdatequizComponent implements OnInit {

  constructor(public dialogRefUpdate: MatDialogRef<UpdatequizComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, public exam:ExamService) { }
  key: string;
  title: string;
  description: string;
  gameon:boolean;
  pin:string;
timelimit:string;
  ngOnInit() {
    this.timelimit = this.data.timelimit
  this.key = this.data.key;
  this.title = this.data.title;
  this.description = this.data.description;
  this.gameon = this.data.gameon;
  this.pin = this.data.pin  
  }

closeDialog(){
  this.dialogRefUpdate.close();
}
updateQuiz(){
  this.exam.updateExam(this.timelimit, this.key,this.title,this.description,this.gameon)
  this.dialogRefUpdate.close();
}

}
