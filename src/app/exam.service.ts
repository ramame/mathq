import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { AuthService } from './auth.service';



@Injectable({
  providedIn: 'root'
})


export class ExamService {
 
  examList: AngularFireList<any>;
  examObject: AngularFireList<any>;
  questionList: AngularFireList<any>;
  
  public examCode: string;
  public examKey:string;


  readForReport(user, examKey){
    this.examList = this.db.list('/Reports/'+user.uid+'/'+examKey+'/students');
    return this.examList.snapshotChanges();

  }
  readExamForReport(user){
    this.examList = this.db.list('/Reports/'+user.uid);
    return this.examList.snapshotChanges();
   
  }
  readthisExamForReport(user , key){
    this.examList = this.db.list('/Reports/'+user.uid+'/'+key);
    return this.examList.snapshotChanges();
   
  }
  
  addExam(timelimit:string,title:string, description:string,code:string, value){
  
  let ref = this.db.database.ref('/');
  
  let reuturnit = ref.child('teacher').child(value.uid).child('exams').push({'title':title, 'description':description, 'code':code, 'gameon':false, 'timeLimit':timelimit})
  return reuturnit;

  
  }

  addQuestion(title:string,From,To , examuid, user){
      let ref = this.db.database.ref('/');
      return ref.child('teacher').child(user.uid).child('exams').child(examuid).child('questions').push({'title':title, 'from':From, 'to':To})
  }

  Exampin(key, title){
    this.examCode = key;
    this.examKey = title;
  }

readExamKey(){
  return this.examCode
}
  readExam(user){
    this.examList = this.db.list('/teacher/'+user.uid+'/exams');
    return this.examList.snapshotChanges();
   
  }
    
  readQuestions(user, key){
    this.questionList = this.db.list('/teacher/'+user.uid+'/exams/'+key+'/questions');
    console.log(this.questionList)
    return this.questionList.snapshotChanges();
   
  }
  DeleteExam(key ,user){
   return this.db.list('/teacher/'+user.uid+'/exams/').remove(key);
  }
  updateExam(timelimit:string, key:string, title:string, description:string, gameon:boolean){
    this.Auth.user.subscribe(user => {
    this.db.list('/teacher/'+user.uid+'/exams/').update(key, {'title':title, 'description':description, 'gameon':gameon, 'timeLimit':timelimit})
    })

      
  }


  activeExam(key:string, gameon:boolean, data:any){
    this.Auth.user.subscribe(user => {
      this.db.list('/teacher/'+user.uid+'/exams/').update(key, {'gameon':gameon});
      let ref = this.db.database.ref('/');
      data["teacherID"] = user.uid;
    //  data.push(y);
      ref.child('gamePIN').child(data.code).push(data);
      
      
      //if ill need test key or question key can i add this code
      //  question.forEach(question => {
     // ref.child('gamePIN').child(data.code).child('questions').push(question)

      //})
     
              
      
      //let ref = this.db.database.ref('/');
      //Here i need to build /pin/pinVar/payload
      //And i need /pin/pinVar/Question/questionPayload
      // And  /pin/pinVar/Index/(1 or 2 or 3..)/student/studentName/title of question will be key// answer will be payload

     // ref.child('GamePIN').child(value.uid).child('exams').child(examuid).child('questions')
    //  .push({'title':title, 'from':From, 'to':To}); 
    })
  }

  endExam(key:string, gameon:boolean){
    this.Auth.user.subscribe(user => {
      this.db.list('/teacher/'+user.uid+'/exams/').update(key, {'gameon':gameon})
      //let ref = this.db.database.ref('/');
      //Here i need to build /pin/pinVar/payload
      //And i need /pin/pinVar/Question/questionPayload
      // And  /pin/pinVar/Index/(1 or 2 or 3..)/student/studentName/title of question will be key// answer will be payload

     // ref.child('GamePIN').child(value.uid).child('exams').child(examuid).child('questions')
    //  .push({'title':title, 'from':From, 'to':To}); 
    })
  }
  DeleteExamFromEntry(key:string){
    this.db.list('/gamePIN/').remove(key);
  }

  updateQuestion(key:string, testkey:string,title:string, from:number, to:number){
    this.Auth.user.subscribe(user => {
    this.db.list('/teacher/'+user.uid+'/exams/'+testkey+'/questions').update(key, {'title':title, 'from':from, 'to':to})
    })     
  }
  deleteQuestion(key:string, testkey:string){
this.Auth.user.subscribe(user => {
  this.db.list('/teacher/'+user.uid+'/exams/'+testkey+'/questions').remove(key);
})
  }
  constructor(private db:AngularFireDatabase, private Auth:AuthService) { }
}

