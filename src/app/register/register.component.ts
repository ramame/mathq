import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import {FormControl, Validators} from '@angular/forms';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
name:string;
email:string;
password:string;
//randomId:number;
school:string;
showmatch: boolean;
Ongoogle:boolean;
Onfacebook:boolean;
repassword:string;

  constructor(public auth:AuthService, public router:Router) { }
 
  ngOnInit() {
    this.school = '';
    this.showmatch = false;
    this.repassword = '';
    this.name ='';
    this.email ='';
    this.password='';
  //  this.randomId = Math.random()*100000
  }
  email1 = new FormControl('', [Validators.required, Validators.email]);
  pass1 =  new FormControl('', [Validators.required, Validators.minLength(6)]);
  pass2 =  new FormControl('', [Validators.required  ]);

  getErrorMessage() {
    return this.email1.hasError('required') ? 'You must enter a value' :
        this.email1.hasError('email') ? 'Not a valid email' :
            '';
  }

  Getrequire(){
    return this.pass1.hasError('required') ? 'You must enter a value' :
    this.pass1.hasError('minlength') ? 'Password should be at least 6 characters' :
    
        '';
  }

  GetMatch(){
      return this.pass1.hasError('required') ? 'You must enter a value' :'';
    }
    checkrapiar(){
      if(this.repassword==this.password){
        this.showmatch = false
      }
    }
    checkmatch(){
      if(!(this.repassword==this.password))
      {
        this.showmatch = true;
      }
      else{
        this.showmatch = false;
      }
     
    }
  in(){
    this.Ongoogle = true;
  }
  out(){
    this.Ongoogle = false
  }
  inF(){
    this.Onfacebook = true;
  }
  outF(){
    this.Onfacebook = false
  }


  toRegister(){
    if (this.password == this.repassword){
    this.auth.RegisterWithEmailandPassword(this.email,this.password).then(value => {
      this.auth.updateUser(value.user,this.name)
      this.auth.addTeacher(value.user,this.name,this.school)
    }).then(value=>{
          this.router.navigate(['/exams']);
      })
    }
    else{
      console.log('need match')
    }
  }

  toRegisterGoogle(){
    
    this.auth.RegisterWithGoogle().then(value => { 
      this.auth.addTeacher1(value.user,value.user.displayName)
      this.router.navigate(['/exams']);
    });
  }
  toRegisterFacebook(){
    this.auth.RegisterWithFacebook().then(value => { 
      this.auth.addTeacher1(value.user,value.user.displayName)
      this.router.navigate(['/exams']);
     })
  }
 






}
