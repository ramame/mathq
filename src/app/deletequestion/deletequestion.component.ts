import { Component, OnInit,  Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ExamService } from '../exam.service';

@Component({
  selector: 'app-deletequestion',
  templateUrl: './deletequestion.component.html',
  styleUrls: ['./deletequestion.component.css']
})
export class DeletequestionComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DeletequestionComponent>, public exam:ExamService, @Inject(MAT_DIALOG_DATA) public data: any) { }
  deleteItem(){
    this.exam.deleteQuestion(this.data.key, this.data.testkey);
    this.dialogRef.close();
 
   }
   closeDialog(){
     this.dialogRef.close();
   }
  ngOnInit() {
  }

}
