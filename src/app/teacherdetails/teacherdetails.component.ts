import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

import { MatDialogRef  } from '@angular/material';

import { Router } from '@angular/router';

@Component({
  selector: 'app-teacherdetails',
  templateUrl: './teacherdetails.component.html',
  styleUrls: ['./teacherdetails.component.css']
})
export class TeacherdetailsComponent implements OnInit {
school:any;
name:any;
realschool:any;
  constructor(public auth:AuthService, public router:Router , public dialogRef: MatDialogRef<TeacherdetailsComponent>) { }
logout(){
  this.auth.logout().then(value => {
   this.router.navigate(['/home']);
   this.dialogRef.close();
  })
  
}
  ngOnInit() {
    this.auth.user.subscribe(value => {
      this.auth.getSchool(value).subscribe(
       schools =>{                       //the snapshot that subscribe return to me.                               
       this.school = [];                
       schools.forEach(                   
       exam => {                            //one of the exams from the snapshot
       let y = exam.payload.toJSON();       //y is a temp var for the payload of specific exam                                    //payload came us string so we need change it to json
                                                      
       this.school.push(y);  
       })
      
     })})
  }

}
