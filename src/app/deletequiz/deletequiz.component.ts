import { Component, OnInit,  Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ExamService } from '../exam.service';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-deletequiz',
  templateUrl: './deletequiz.component.html',
  styleUrls: ['./deletequiz.component.css']
})
export class DeletequizComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DeletequizComponent>, public exam:ExamService, @Inject(MAT_DIALOG_DATA,) public data: any, private Auth:AuthService
  ,public router:Router) { }

  ngOnInit() {
  }
  deleteItem(){
    this.Auth.user.subscribe(user =>{
      this.exam.DeleteExam(this.data.key, user).then(value =>{
        this.router.navigate(['/exams']);
     
      })
     
    })
 
   this.dialogRef.close();

  }
  closeDialog(){
    this.dialogRef.close();
  }
}
