import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { Router } from '@angular/router';
import { ExamService } from '../exam.service';

@Component({
  selector: 'app-exams',
  templateUrl: './exams.component.html',
  styleUrls: ['./exams.component.css']
})
export class ExamsComponent implements OnInit {
exams =[];
panelOpenState = false;

  constructor( public Auth:AuthService, public db:AngularFireDatabase, public router:Router, public exam:ExamService) { }

  createExam(){
    this.router.navigate(['/cexam']);
  }

  ngOnInit() {
   //read exams by servise
    this.Auth.user.subscribe(value => {
    this.exam.readExam(value).subscribe(
    exams =>{                       //the snapshot that subscribe return to me.                               
    this.exams = [];                
    exams.forEach(                   
    exam => {                            //one of the exams from the snapshot
    let y = exam.payload.toJSON();       //y is a temp var for the payload of specific exam                                    //payload came us string so we need change it to json
    y["key"] = exam.key;                //we will want here a pin                                         //key מילה שמורה
    this.exams.push(y);  
    })
    })})
  }





}
