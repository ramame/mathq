import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnterpinComponent } from './enterpin.component';

describe('EnterpinComponent', () => {
  let component: EnterpinComponent;
  let fixture: ComponentFixture<EnterpinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnterpinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnterpinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
