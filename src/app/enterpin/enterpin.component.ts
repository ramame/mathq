import { Component, OnInit } from '@angular/core';
import { StudentsideService } from '../studentside.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-enterpin',
  templateUrl: './enterpin.component.html',
  styleUrls: ['./enterpin.component.css']
})
export class EnterpinComponent implements OnInit {
pin:string ;
exams:any = [];
flag = 0 ;
showError:boolean;
  constructor(public studentServise:StudentsideService, public router:Router) { }
  

  ngOnInit() {
    this.showError = false
  }

    findTest(){
    

      this.studentServise.findExam(this.pin).subscribe(exam =>{
      //the snapshot that subscribe return to me.  
     if(exam.length == 0){
      this.showError = true;
     }
     else{
      this.flag = 1; 
     }
                                
      this.exams = [];                
      exam.forEach(                   
      exam => {                          
      let y = exam.payload.toJSON();       
      y["key"] = exam.key;                
      this.exams.push(y);  
      })})
      
        this.delay(1000).then(value => {
        this.studentServise.saveExam(this.exams, this.pin, this.exams[0].key);
      }).then(value => {
        if(this.exams.length != 0){ 
        this.router.navigate(['/studentname']);
      }
      else{
        console.log('3')
      }
      })
   

    }

    delay(ms: number) {
      return new Promise( resolve => setTimeout(resolve, ms) );
  }
//now i have in exams all the payload as json
}
