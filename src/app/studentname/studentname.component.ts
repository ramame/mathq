import { Component, OnInit } from '@angular/core';
import { StudentsideService } from '../studentside.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-studentname',
  templateUrl: './studentname.component.html',
  styleUrls: ['./studentname.component.css']
})
export class StudentnameComponent implements OnInit {
name:string;
  constructor(public studentServise:StudentsideService, public router:Router) { }
  

  enterName(){
   this.studentServise.addName(this.name).then(value =>{
     this.studentServise.saveKeyName(value.key)
    this.router.navigate(['/studenttest']);
   })
  } 
  
  ngOnInit() {
  }

}
