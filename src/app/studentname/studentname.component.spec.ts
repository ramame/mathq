import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentnameComponent } from './studentname.component';

describe('StudentnameComponent', () => {
  let component: StudentnameComponent;
  let fixture: ComponentFixture<StudentnameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentnameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentnameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
