import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CquestionComponent } from './cquestion.component';

describe('CquestionComponent', () => {
  let component: CquestionComponent;
  let fixture: ComponentFixture<CquestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CquestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CquestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
