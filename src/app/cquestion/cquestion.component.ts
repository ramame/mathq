import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from '../auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { ExamsComponent } from '../exams/exams.component';
import { ExamService } from '../exam.service';
import {FormControl, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { DeletequizComponent } from '../deletequiz/deletequiz.component';
import { MatDialog } from '@angular/material';
@Component({
  selector: 'app-cquestion',
  templateUrl: './cquestion.component.html',
  styleUrls: ['./cquestion.component.css']
})
export class CquestionComponent implements OnInit {
  
que_title='';
from:any;
to:any;
code:string;
questions = [];
exams =[];
Froming = new FormControl('', [ Validators.pattern("^[0-9]*$")]);
Toing = new FormControl('', [ Validators.pattern("^[0-9]*$")]);

  constructor(public Auth:AuthService, private db:AngularFireDatabase, private exam:ExamService, private router:Router,public dialog: MatDialog) { }


  getErrorMessageFrom() {
    return  this.Froming.hasError('pattern') ? 'Please enter  a numeric value' :'';
  }
  getErrorMessageTo(){
    return   this.Toing.hasError('pattern') ? 'Please enter a numeric value' :'';
  }
  deleteExam(){
 
      const dialogRef = this.dialog.open(DeletequizComponent, {
        data: {key: this.exam.examCode,  name: this.exam.examKey}
      });
    
    
  }
  saveAndGo(){
        this.router.navigate(['/exams']);
  }

  addQuestion(){
    if(!(this.que_title == '')&&!(isNaN(this.to))&&!(isNaN(this.from))){ 
    this.Auth.user.subscribe(value => {
      this.exam.addQuestion(this.que_title,this.from,this.to, this.exam.examCode, value).then(value =>{
        console.log(value);
        this.que_title='';
this.from ='';
this.to='';
      })
    })
  }
  else{
    alert('Please try again, Note: the "Currect Range" need to be numbers.')
  }

  }


  ngOnInit() {
    this.code = this.exam.readExamKey();
    this.Auth.user.subscribe(value => {
      this.exam.readQuestions(value, this.exam.examCode).subscribe(
       questions =>{                       //the snapshot that subscribe return to me.                               
       this.questions = [];             
       questions.forEach(                   
       question => {                            //one of the exams from the snapshot
       let y = question.payload.toJSON();       //y is a temp var for the payload of specific exam                                    //payload came us string so we need change it to json
       y["key"] = question.key; 
       y["testkey"] =this.exam.examCode;               //we will want here a pin                                         //key מילה שמורה
       this.questions.push(y);  
       })
     })}) 

   this.Auth.user.subscribe(user => {
    this.db.list('/teacher/'+user.uid+'/exams').snapshotChanges().subscribe(
      exams =>{                       //the snapshot that subscribe return to me.                               
      this.exams = [];                
      exams.forEach(                   
      exam => {                            //one of the exams from the snapshot
      let y = exam.payload.toJSON();       //y is a temp var for the payload of specific exam                                    //payload came us string so we need change it to json
      y["$key"] = exam.key;                //we will want here a pin                                         //key מילה שמורה
      this.exams.push(y);                  //מכניס את האיבר שיצרתי בתוך טודוס שלנו
      

    })})}) 
}





}
