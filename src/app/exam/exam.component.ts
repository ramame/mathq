import { Component, OnInit , Input} from '@angular/core';
import { AuthService } from '../auth.service';
import { ExamService } from '../exam.service';
import { MatDialog } from '@angular/material';
import { DeletequizComponent } from '../deletequiz/deletequiz.component';
import { UpdatequizComponent } from '../updatequiz/updatequiz.component';
@Component({
  selector: 'app-exam',
  templateUrl: './exam.component.html',
  styleUrls: ['./exam.component.css']
})
export class ExamComponent implements OnInit {
  @Input() data:any;
  questions = [];
  title;
  description;
  PIN;
  gameon;
  gamestatus = '';
  key;
  timelimit;
  //img is an option
  constructor(public auth:AuthService, public exam:ExamService, public dialog: MatDialog) { }

Onopen(){
  console.log('opened')

}
Onclose(){
  console.log('closed')
}

openDeleteDialog(){
  const dialogRef = this.dialog.open(DeletequizComponent, {
    data: {key: this.key,  name: this.title}
  });

}

activeExam(){
  this.gameon = true;
  this.exam.activeExam(this.key,this.gameon,this.data) 
  console.log(this.questions);
  //data in json array
  //questions is array of json 0:{{title: 1}} 1:{{title: 2}} 2:{{title: 3}}
  console.log(this.data);
  //active exam open pin
}

endExam(){
  this.gameon = false;
  this.exam.endExam(this.key,this.gameon)
  this.exam.DeleteExamFromEntry(this.key)
  //end exam need to think..
}

openUpdateDialog(): void {
  const dialogRefUpdate = this.dialog.open(UpdatequizComponent, {
    width: '400px',
    data: {key: this.key ,title: this.title, description: this.description, gameon: this.gameon, pin: this.PIN, timelimit:this.timelimit}
  });

  dialogRefUpdate.afterClosed().subscribe(result => {
    console.log('The dialog was closed');
    console.log(result);
  });
}

  ngOnInit() {
     this.timelimit = this.data.timeLimit;
     this.gameon = this.data.gameon;
     if(this.gameon == true){
      this.gamestatus = 'game is active'
    }
     this.title = this.data.title;
     this.description = this.data.description;
     this.PIN = this.data.code;
     this.key = this.data.key;
      //here read questions
     this.auth.user.subscribe(value => {
      this.exam.readQuestions(value, this.key).subscribe(
       questions =>{                       //the snapshot that subscribe return to me.                               
       this.questions = [];             
       questions.forEach(                   
       question => {                            //one of the exams from the snapshot
       let y = question.payload.toJSON();       //y is a temp var for the payload of specific exam                                    //payload came us string so we need change it to json
       y["key"] = question.key; 
       y["testkey"] =this.key;               //we will want here a pin                                         //key מילה שמורה
       this.questions.push(y);  
       })
     })})
     }
   
   
   
   
   
   }
   