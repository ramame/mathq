import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 
import {MatDialogModule} from '@angular/material/dialog';
import { HttpClientModule } from '@angular/common/http';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import {environment} from '../environments/environment';



import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { AppComponent } from './app.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { ExamsComponent } from './exams/exams.component';
import {MatIconModule} from '@angular/material/icon';
import { Routes,RouterModule } from '@angular/router';
import { NavComponent } from './nav/nav.component';
import {MatCardModule} from '@angular/material/card';
import {MatDividerModule} from '@angular/material/divider';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import { ExamComponent } from './exam/exam.component';
import { CexamComponent } from './cexam/cexam.component';
import { CquestionComponent } from './cquestion/cquestion.component';
import { TeacherdetailsComponent } from './teacherdetails/teacherdetails.component';
import {MatListModule} from '@angular/material/list';
import {MatExpansionModule} from '@angular/material/expansion';
import { DeletequizComponent } from './deletequiz/deletequiz.component';
import { UpdatequizComponent } from './updatequiz/updatequiz.component';
import { QuestionComponent } from './question/question.component';
import { DeletequestionComponent } from './deletequestion/deletequestion.component';
import { EnterpinComponent } from './enterpin/enterpin.component';
import { TestrunComponent } from './testrun/testrun.component';
import { StudentnameComponent } from './studentname/studentname.component';
import {MatStepperModule} from '@angular/material/stepper';
//angular bootstap
import { AlertModule } from 'ngx-bootstrap';
import { ReportsComponent } from './reports/reports.component';
import { TyexamComponent } from './tyexam/tyexam.component';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatTableModule} from '@angular/material/table';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { ContectComponent } from './contect/contect.component';
@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    HomeComponent,
    ExamsComponent,
    NavComponent,
    ExamComponent,
    CexamComponent,
    CquestionComponent,
    TeacherdetailsComponent,
    DeletequizComponent,
    UpdatequizComponent,
    QuestionComponent,
    DeletequestionComponent,
    EnterpinComponent,
    TestrunComponent,
    StudentnameComponent,
    ReportsComponent,
    TyexamComponent,
    ContectComponent,
  ],

  imports: [
    AlertModule.forRoot(),
    MatDialogModule,
    MatCardModule,
    MatTableModule,
    MatGridListModule,
    MatSnackBarModule,
    MatStepperModule,
    MatExpansionModule,
    MatCheckboxModule,
    MatListModule,
    ReactiveFormsModule,
    BrowserModule,
    MatIconModule,
    HttpClientModule, 
    MatDividerModule,
    MatInputModule,
    FormsModule,
    MatFormFieldModule,
    BrowserAnimationsModule,
    MatButtonModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    RouterModule.forRoot([
      {path:'login', component:LoginComponent},
      {path:'cquestion', component:CquestionComponent},
      {path:'cexam', component:CexamComponent},
      {path:'register', component:RegisterComponent},   
      {path:'home', component:HomeComponent},
      {path:'exams', component:ExamsComponent},
      {path:'gamepin', component:EnterpinComponent},
      {path:'studenttest', component:TestrunComponent},
      {path:'studentname', component:StudentnameComponent},
      {path:'reports', component:ReportsComponent},
      {path:'report', component:ReportsComponent},
      {path:'**', component:LoginComponent},
      
    ])

  ],

  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    TeacherdetailsComponent,
    DeletequizComponent,
    UpdatequizComponent,
    DeletequestionComponent,
    ContectComponent,
  ]
})
export class AppModule { }
