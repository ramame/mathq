import {Component,OnInit, Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ContectComponent } from '../contect/contect.component';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  contact(){
    const dialogRef = this.dialog.open(ContectComponent, {
      width: '250px',
  
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
  
    });
  
 
  }

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
    
  }

}
