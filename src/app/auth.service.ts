import { Injectable } from '@angular/core';

import {AngularFireAuth} from '@angular/fire/auth'
import {Observable} from 'rxjs'
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';
import * as firebase from 'firebase/app';
import { PathLocationStrategy } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  school: AngularFireList<any>;
  user:Observable<firebase.User>;
  constructor(private fireAuth:AngularFireAuth, 
    private db:AngularFireDatabase) {
      this.user = fireAuth.authState;
     }
loginwithEmailPassword(Email:string, Password:string){
  return this.fireAuth.auth.signInWithEmailAndPassword(Email,Password);
}

  logout(){
  return this.fireAuth.auth.signOut() 
  }
 loginG(){
   return this.fireAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
 }
 loginF(){
   return this.fireAuth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider());
 }
  RegisterWithEmailandPassword(email,password){
    return this.fireAuth.auth.createUserWithEmailAndPassword(email,password);
  }

  RegisterWithGoogle(){
    return new Promise<any>((resolve, reject) => {
      let provider = new firebase.auth.GoogleAuthProvider();
      provider.addScope('profile');
      provider.addScope('email');
      this.fireAuth.auth
      .signInWithPopup(provider)
      .then(res => {
        resolve(res);
          })
        })
  }
  
  RegisterWithFacebook(){
    return new Promise<any>((resolve, reject) => {
      let provider = new firebase.auth.FacebookAuthProvider();
      this.fireAuth.auth
      .signInWithPopup(provider)
      .then(res => {
        resolve(res);
      }, err => {
        console.log(err);
        reject(err);
      })
    })
  }

  updateUser(user,name:string){
    return user.updateProfile({displayName:name,photoURL:'assets/picture/user.png'})
  }
  
  addTeacher1(user, name:string){               //המילה צ'ילד מחפשת אינפוט אם הוא מוצא הוא כותב תחתיו ומתקדם אם הוא לא מוצא הוא מוסיף את האיי די וממשיך את הפקודות
  let uid = user.uid;                          // לדוגמא אם היה דטה בייס ריק היה ברגע ההרצה של הקוד הזה היה נוצר המילה יוסרס ומתחתיו האיי די של המשתמש שיוצר ומתחתיו השם משתמש של אותו  משתמש            
  let ref = this.db.database.ref('/');
  ref.child('teacher').child(uid).push({'name':name});  //"name" שם התכונה
                                                        //name בלי גרשיים יהיה השם שהעברנו
                                                        //כי אנחנו כותבים פה גייסון
                                                        // הפונקציה פוש מוסיפה קיי חדש בדטה בייס    
  }
  


  addTeacher(user: firebase.User, name:string, school:string){    
     
 let uid = user.uid;                          // לדוגמא אם היה דטה בייס ריק היה ברגע ההרצה של הקוד הזה היה נוצר המילה יוסרס ומתחתיו האיי די של המשתמש שיוצר ומתחתיו השם משתמש של אותו  משתמש            
 let ref = this.db.database.ref('/');
 return ref.child('teacher').child(user.uid).push({'name':name, 'school':school});  //"name" שם התכונה
                                                                                                          //name בלי גרשיים יהיה השם שהעברנו
                                                                                                          //כי אנחנו כותבים פה גייסון
                                                                                                          // הפונקציה פוש מוסיפה קיי חדש בדטה בייס
                                                                                                      }
                                                    
  getSchool(user){
    this.school = this.db.list('/teacher/'+user.uid);
    return this.school.snapshotChanges();
   
  }                                        
                                                  
                                                  



}

