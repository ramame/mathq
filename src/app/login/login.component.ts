import { Component, OnInit } from '@angular/core';
import { ExamComponent } from '../exam/exam.component';
import { ExamService } from '../exam.service';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
name;
exams = [];
Ongoogle;
Onfacebook;
email = '';
password = '';
  constructor(public exam:ExamService, public auth:AuthService, public router:Router) { }

  ngOnInit() {
    this.Ongoogle = false;
    this.Onfacebook = false;
  }

  
  in(){
    this.Ongoogle = true;
  }
  out(){
    this.Ongoogle = false
  }
  inF(){
    this.Onfacebook = true;
  }
  outF(){
    this.Onfacebook = false
  }

  LoginG(){
      this.auth.loginG().then(user => {
      this.router.navigate(['/exams']);
      console.log(user);
      console.log(user.user)
      });
  }
    LoginF(){
      this.auth.loginF().then(user => {
      this.router.navigate(['/exams']);
      console.log(user);
      console.log(user.user)
      });
  }
  loginWithEmailpass(){
    this.auth.loginwithEmailPassword(this.email,this.password).then(user => {
    this.router.navigate(['/exams']);
      console.log(user);
    })
  }




}
