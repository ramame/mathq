import { TestBed } from '@angular/core/testing';

import { StudentsideService } from './studentside.service';

describe('StudentsideService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StudentsideService = TestBed.get(StudentsideService);
    expect(service).toBeTruthy();
  });
});
