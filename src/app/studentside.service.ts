import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { AuthService } from './auth.service';
import { stringify } from 'querystring';
import { Title } from '@angular/platform-browser';
import { from } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class StudentsideService {

  constructor(private db:AngularFireDatabase, private Auth:AuthService) { }
   examjson:  {code:any, description:any, gameon:any, participants:any, questions:any, timeLimit:any, title: any; teacherID: any; key: any; };
   examList: AngularFireList<any>;
   gamepin:string;
   questionList: AngularFireList<any>;
   key:string;
   ParticipantsList: AngularFireList<any>;
   myName:string;
   keyForName:string;
 
   findExam(pin:string){
     console.log('here')
    this.examList = this.db.list('/gamePIN/'+pin);
    return this.examList.snapshotChanges();

   }

   saveExam(exam:{code:any, description:any, gameon:any, participants:any, questions:any, timeLimit:any, title: any; teacherID: any; key: any; }, pin: string, key: string){
    this.key = key;
    this.gamepin = pin;
    this.examjson = exam;

   } 

   readQuestions(){
    // i can send the pin exams.code from enterpin component
    this.questionList = this.db.list('/gamePIN/'+this.gamepin+'/'+this.key+'/questions/');
    return this.questionList.snapshotChanges(); 
   }
   readParticipants(){
     this.ParticipantsList = this.db.list('/gamePIN/'+this.gamepin+'/'+this.key+'/participants/');
     return this.ParticipantsList.snapshotChanges();
   }
    addName(name:string){    
      this.myName = name;
      console.log(name)
      let ref = this.db.database.ref('/');
      //if name is not taken before (add function here)
      //here i want start time
     return ref.child('gamePIN').child(this.gamepin).child(this.key).child('participants').push({'name':name, 'on': true})
    }
    TimeForUser(time){
    this.db.list('/gamePIN/'+this.gamepin+'/'+this.key+'/participants').update(this.keyForName, {'TimeIn':time }) 
    //here i have the pass time 1 2 3 ..
    }
    saveKeyName(key:string){
     this.keyForName = key
    }
    logOutName(){
      let ref = this.db.database.ref('/');
      console.log('logOutName')
      console.log('/gamePIN/'+this.gamepin+'/'+this.key+'/participants'+this.keyForName)

      //if name is not taken before (add function here)
      //add key
     return ref.child('gamePIN').child(this.gamepin).child(this.key).child('participants').child(this.keyForName).update({'name':name, 'on': false})
    }
    saveAnswers(questionAns: { answer: any; title: any; from: any; to: any; }){
      let ref = this.db.database.ref('/');
      console.log('logOutName')
      console.log('/gamePIN/'+this.gamepin+'/'+this.key+'/participants'+this.keyForName)
      return ref.child('gamePIN').child(this.gamepin).child(this.key).child('participants').child(this.keyForName).child('answers').push({ 'answer' : questionAns.answer, 'title':questionAns.title , 'from': questionAns.from, 'to':questionAns.to})
    }


    //update if not exist does create.
    
    observeCount(time){
      let ref = this.db.database.ref('/');
      console.log('logOutName')
      console.log('/gamePIN/'+this.gamepin+'/'+this.key+'/participants'+this.keyForName)

      //if name is not taken before (add function here)
      //add key
     return ref.child('names').update({'on': false})
    }
    saveAnswersForReport(examName, teacherID, examKey, studentName:string, questionAns: { answer: any; title: any; from: any; to: any; },counter,TimeLeft,endTime,numOfQuestions ){
      //report -> teacherID -> examKey -> students -> name -> answers
      let ref = this.db.database.ref('/');
      this.db.list('/Reports/'+teacherID).update(examKey, {'examName':examName, 'numQue':numOfQuestions, 'endTime':endTime })
      ref.child('Reports').child(teacherID).child(examKey).child('questions').child(questionAns.title).child('students').child(studentName).push({ 'answer' : questionAns.answer, 'title':questionAns.title , 'from': questionAns.from, 'to':questionAns.to})
      return this.db.list('/Reports/'+teacherID+'/'+examKey+'/students').update(studentName, {'counter':counter, 'timeLeft':TimeLeft})
      
    }
   
   
}
